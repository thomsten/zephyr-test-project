# Zephyr test project

This is a simple project to show how an automated test framework could be implemented using the [Zephyr RTOS](https://github.com/zephyrproject-rtos/zephyr) and Python's [`unittest`](https://docs.python.org/3/library/unittest.html) framework.
It assumes the use of a Nordic nRF Series platform as the embedded test device, but can be easily adopted to any other board with a serial interface.

## Building and running

1. Clone this repository into your Zephyr workspace, e.g.

        git clone https://gitlab.com/thomsten/zephyr-test-project ~/zephyr/samples/

2. Build and flash the program

        zephyr-test-project $ west flash --erase --board nrf52840dk_nrf52840

3. Run the test script

        zephyr-test-project $ python -m unittest scripts/test_program.py

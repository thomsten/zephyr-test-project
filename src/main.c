#include <zephyr.h>
#include <sys/printk.h>


static void assert_equal_int(int exp, int val) {
	if (exp != val) {
		printk("ASSERT: Expected %d, != %d\n", exp, val);
	}
}

typedef int (*test_func_t)(void);

static int test_success(void) {
	if (2 + 2 != 4) {
		return -1;
	}
	return 0;
}

static int test_fail(void) {
	return -1;
}

const test_func_t TEST_CASES[] = {
	test_success,
	test_fail
};

void main(void)
{
	for (int i = 0; i < sizeof(TEST_CASES[0])/sizeof(TEST_CASES); ++i) {
		const int result = TEST_CASES[i]();
		assert_equal_int(0, result);
	}

	printk("Test complete\n");
}

#!/usr/bin/env python3
#
# Copyright (c) Thomas Stenersen
# SPDX-License-Identifier: MIT

import unittest
import subprocess
import logging
import time
import serial


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class TestProgram(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._serial = None
        self._port = '/dev/ttyACM0'
        self._baudrate = 115200
        self._flow_control = True

    def setUp(self):
        logger.debug('Setting up')
        logger.debug('Flushing serial buffers')
        subprocess.check_output(['nrfjprog', '--halt'])
        self._serial = serial.Serial(port=self._port,
                                     baudrate=self._baudrate,
                                     rtscts=self._flow_control)
        self._serial.read_all()
        logger.debug('Resetting...')
        subprocess.check_output(['nrfjprog', '--reset'])

    def tearDown(self):
        self._serial.close()
        self._serial = None
        subprocess.check_output(['nrfjprog', '--halt'])

    def test_program(self):
        wait_time_s = 1
        logger.info(f'Waiting {wait_time_s}s for test to complete...')
        time.sleep(wait_time_s)

        serial_out = self._serial.read_all().decode('utf-8')

        self.assertIsNotNone(serial_out)
        self.assertNotIn('assert', serial_out.lower())

        logger.info('Test complete')

        logger.debug('BEGIN_OUTPUT')
        logger.debug(serial_out)
        logger.debug('END_OUTPUT')
